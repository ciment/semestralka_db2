﻿alter table typ_merania drop column popis;
alter table typ_merania drop column jednotka;
alter table typ_merania add popis varchar2(20) not null;
alter table typ_merania add jednotka varchar2(15) not null;

insert into typ_merania (id_typ,popis,interval,jednotka,kriticka_dolna,kriticka_horna,chybna_dolna,chybna_horna)
values (1,'teplota',6*3600,'C',35,38,30,42);

insert into typ_merania (id_typ,popis,interval,jednotka,kriticka_dolna,kriticka_horna,chybna_dolna,chybna_horna)
values (2,'systolicky_tlak',1*3600,'mm/Hg',80,150,0,200);

insert into typ_merania (id_typ,popis,interval,jednotka,kriticka_dolna,kriticka_horna,chybna_dolna,chybna_horna)
values (3,'diastolicky_tlak',1*3600,'m/Hg',40,100,0,200);

insert into typ_merania (id_typ,popis,interval,jednotka,kriticka_dolna,kriticka_horna,chybna_dolna,chybna_horna)
values (4,'pulz',60,'min',40,100,0,300);

insert into typ_merania (id_typ,popis,interval,jednotka,kriticka_dolna,kriticka_horna,chybna_dolna,chybna_horna)
values (5,'sedimentacia',24*3600,'mm',2,8,0,50);

insert into typ_merania (id_typ,popis,interval,jednotka,kriticka_dolna,kriticka_horna,chybna_dolna,chybna_horna)
values (6,'leukocyty',24*3600,'10na9*ℓ–1',4,8,0,50);


insert into typ_merania (id_typ,popis,interval,jednotka,kriticka_dolna,kriticka_horna,chybna_dolna,chybna_horna)
values (7,'erytrocyty',24*3600,'10na12*ℓ–1',4.3,5.7,0,50);


insert into vysetrenie (id_vysetrenie,popis) VALUES (1,'krvne_vysledky');
insert into vysetrenie (id_vysetrenie,popis) VALUES (2,'tlak');

commit;


