CREATE OR REPLACE TRIGGER check_range BEFORE INSERT ON meranie
  FOR EACH ROW
  DECLARE
  k_dolna float ;
      k_horna float ;
      ch_dolna float ;
      ch_horna float ;
  BEGIN
  
  SELECT kriticka_dolna,kriticka_horna,chybna_dolna,chybna_horna into k_dolna,k_horna,ch_dolna,ch_horna 
    FROM typ_merania WHERE id_typ=:new.id_typ; 
    
    IF :new.hodnota<=ch_dolna OR :new.hodnota>=ch_horna THEN
      :new.stav:='E';
    ELSIF :new.hodnota<=k_dolna OR :new.hodnota>=k_horna THEN 
      :new.stav:='C';
    ELSE 
      :new.stav:='G';
    END IF;
    
    IF :new.stav = 'E' THEN
      INSERT INTO log (cas,id_pacient,id_typ,id_vysetrenie,stav,hodnota)
      VALUES(:new.cas,:new.id_pacient,:new.id_typ,:new.id_vysetrenie,:new.stav,:new.hodnota);
    END IF;
    
  END;
  /