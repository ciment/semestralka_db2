package DatabaseCore;



import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import oracle.ucp.jdbc.PoolDataSource;
import oracle.ucp.jdbc.PoolDataSourceFactory;

    
    
public class DBConnection {
    
    private static  final DBConnection INSTANCE =new DBConnection();
    
    private PoolDataSource pds=null;
    
    
    private DBConnection(){
        try {
            pds = PoolDataSourceFactory.getPoolDataSource();
            pds.setConnectionFactoryClassName("oracle.jdbc.pool.OracleDataSource");
            pds.setURL("jdbc:oracle:thin:@asterix.fri.uniza.sk:1521:orcl");
            pds.setUser("hriadel");
            pds.setPassword("asdfg");
            pds.setInitialPoolSize(5);
        } catch (SQLException ex) {
            System.out.println("OracleConnection - "
                    + "SQLException occurred : "
                    + ex.getMessage());
        }
    }
  
    public static DBConnection getInstance() throws SQLException {
          return INSTANCE;
        }

    
    public  Connection getConnection() throws SQLException{
        
       return pds.getConnection();
    }

}