/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DatabaseCore;

import Entity.Meranie;
import Entity.Pacient;
import Entity.TypMerania;
import Entity.Vysetrenie;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import oracle.jdbc.OracleTypes;

/**
 *
 * @author rthr
 */
public class DBgetData {

    public static List<Pacient> getPacients(Connection con)
            throws SQLException {
        
        ResultSet rs = null;
        CallableStatement cs = null;
        List<Pacient> pacients = new ArrayList<>();

        try {
            cs = con.prepareCall("{call GET_PACIENTS(?)}");
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.executeUpdate();
            rs = (ResultSet) cs.getObject(1);
            while (rs.next()) {
                int id = rs.getInt("id_pacient");
                String name = rs.getString("meno");
                String surname = rs.getString("priezvisko");
                String rc = rs.getString("rod_cislo");

                pacients.add(new Pacient(id, name, surname, rc));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }

            if (cs != null) {
                cs.close();
            }

            if (con != null) {
                con.close();
            }
        }

        return pacients;
    }
    
    public static List<TypMerania> getTypyMerania(Connection con) throws SQLException {
        ResultSet rs = null;
        CallableStatement cs = null;
        List<TypMerania> merania = new ArrayList<>();
        try {
            cs = con.prepareCall("{call GET_TYPY_MERANI(?)}");
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.executeUpdate();
            rs = (ResultSet) cs.getObject(1);
            while (rs.next()) {
                int id = rs.getInt("id_typ");
                String popis = rs.getString("popis");
                int interval = rs.getInt("interval");
                String jednotka = rs.getString("jednotka");
                double kritickaDolna = rs.getDouble("kriticka_dolna");
                double kritickaHorna = rs.getDouble("kriticka_horna");
                double chybnaDolna = rs.getDouble("chybna_dolna");
                double chybnaHorna = rs.getDouble("chybna_horna");
               merania.add(new TypMerania(id, popis, interval, jednotka, kritickaDolna,
                        kritickaHorna, chybnaDolna, chybnaHorna));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cs != null) {
                cs.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return merania;
    }

    public static List<Meranie> getMerania(Connection con, int id_pac, int id_mer)
            throws SQLException {

        ResultSet rs = null;
        CallableStatement cs = null;
        List<Meranie> merania = new ArrayList<>();

        try {
            cs = con.prepareCall("{call GET_MERANIA(?,?,?)}");
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.setInt(2, id_pac);
            cs.setInt(3, id_mer);
            cs.executeUpdate();
            rs = (ResultSet) cs.getObject(1);
            while (rs.next()) {

                Date cas = rs.getDate("cas");
                //int id_pacient = rs.getInt("id_pacient");
                //int id_typ = rs.getInt("id_typ");
                //int id_vysetrenie = rs.getInt("id_vysetrenie");
                String stav = rs.getString("stav");
                float hodnota = rs.getFloat("hodnota");


                merania.add(new Meranie(cas, stav, hodnota));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cs != null) {
                cs.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return merania;
    }
    
      public static List<Meranie> getMeraniaDatum(Connection con, int id_pac, int id_mer,Date dateFrom,Date dateTo)
            throws SQLException {

        ResultSet rs = null;
        CallableStatement cs = null;
        List<Meranie> merania = new ArrayList<>();
        if(dateFrom==null){
            dateFrom=new java.sql.Date(0, 1, 1);
        }
        if(dateTo==null){
            dateTo=new java.sql.Date(200, 1, 1);
        }
      
        try {
            cs = con.prepareCall("{call GET_MERANIA_DATUM(?,?,?,?,?)}");
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.setInt(2, id_pac);
            cs.setInt(3, id_mer);
             cs.setDate(4, convertToSQLDate(dateFrom));
             cs.setDate(5, convertToSQLDate(dateTo));
            cs.executeUpdate();
            rs = (ResultSet) cs.getObject(1);
            while (rs.next()) {

                Date cas = rs.getDate("cas");
                //int id_pacient = rs.getInt("id_pacient");
                //int id_typ = rs.getInt("id_typ");
                //int id_vysetrenie = rs.getInt("id_vysetrenie");
                String stav = rs.getString("stav");
                float hodnota = rs.getFloat("hodnota");


                merania.add(new Meranie(cas, stav, hodnota));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cs != null) {
                cs.close();
            }
            if (con != null) {
                con.close();
            }
        }

        return merania;
    }

    
    public static List<Vysetrenie> getVysetrenia(Connection con)
    throws SQLException {

    ResultSet rs = null;
        CallableStatement cs = null;
    
    List<Vysetrenie> vysetrenie = new ArrayList<>();

    try {
          cs = con.prepareCall("{call GET_VYSETRENIA(?)}");
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.executeUpdate();
            rs = (ResultSet) cs.getObject(1);
        while (rs.next()) {
                        
            int id = rs.getInt("id_vysetrenie");
            String popis = rs.getString("popis");
            
            
            vysetrenie.add(new Vysetrenie(id, popis));
        }
    } catch (SQLException e ) {
        e.printStackTrace();
    } finally {
          if (rs != null) {
                rs.close();
            }
            if (cs != null) {
                cs.close();
            }
            if (con != null) {
                con.close();
            }
    }
    
    return vysetrenie;
}
    
    public static Integer selectMaxIdPacient(Connection con) throws SQLException {


        ResultSet rs = null;
        CallableStatement cs = null;
        Integer maxId = null;
        try {
            cs = con.prepareCall("{call GET_MAX_ID_PACIENT(?)}");
            cs.registerOutParameter(1, OracleTypes.CURSOR);
            cs.executeUpdate();
            rs = (ResultSet) cs.getObject(1);
            if (rs.next()) {
                maxId = rs.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cs != null) {
                cs.close();
            }
            if (con != null) {
                con.close();
            }
            return (maxId == null) ? 0 : maxId;
        }

    }
    
    public static void insertPacient(Pacient pacient,Connection con) throws SQLException{
        
         PreparedStatement stmt = null;
        String query = "INSERT INTO pacient(id_pacient,meno,priezvisko,rod_cislo) VALUES (?,?,?,?)";
        Integer maxId = null;
        try {
            stmt = con.prepareStatement(query);
            stmt.setInt(1, pacient.getId());
            stmt.setString(2, pacient.getName());
            stmt.setString(3, pacient.getSurname());
            stmt.setString(4, pacient.getRC());
                    
            ResultSet rs = stmt.executeQuery();
           
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (con != null) {
                con.close();
            }
        
        }
    }
    
    
    
    public static java.sql.Date convertToSQLDate(Date date) {

        java.util.Calendar cal = Calendar.getInstance();

        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        java.sql.Date sqlDate = new java.sql.Date(cal.getTime().getTime()); // your sql date
       return  sqlDate;

    }
    
}
