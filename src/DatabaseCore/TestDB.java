package DatabaseCore;


import DatabaseCore.DBConnection;
import DatabaseCore.DBConnection;

import Entity.Pacient;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Ciment
 */
public class TestDB {
    

    
    
    public static void main(String []args) throws SQLException{
        DBConnection orcl = DBConnection.getInstance();
        
        Connection con=orcl.getConnection();
        viewTable(con, "pacient");
        
        Generators.PersonGenerator.generatePerson(10);
        
        Integer selectMaxIdPacient = DBgetData.selectMaxIdPacient(con);
        
        System.out.println(selectMaxIdPacient);
        
        
                
        
        
    }
    
    
    public static void viewTable(Connection con, String tableName)
    throws SQLException {

    Statement stmt = null;
    String query =
        "SELECT *"
            + " FROM " + tableName ;

    try {
        stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        while (rs.next()) {
            String name = rs.getString("meno");
            String surname=rs.getString("priezvisko");
            System.out.println(name + "\t" + surname);
        }
    } catch (SQLException e ) {
        e.printStackTrace();
    } finally {
        if (stmt != null) { stmt.close(); }
    }
}
    
    
    
    
    

}
