/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Generators;

import DatabaseCore.DBConnection;
import DatabaseCore.DBgetData;
import Entity.Pacient;
import java.sql.SQLException;
import java.util.Random;

/**
 *
 * @author Ciment
 */
public class PersonGenerator {

    public static String[] listNames = {"Jozef", "Jano", "Fero", "Miso", "Juraj", "Miroslav", "Peter", "Pavol", "Milos", "Zdeno", "Tomas"};
    public static String[] listSurname = {"Dlhy", "Kratky", "MIly", "Mokry", "Vesely", "Smutny", "Pagac", "Smotana", "Kyblik", "Novak", "Strapaty"};




    public static void generatePerson(int numberPerson) throws SQLException {
        
        Random initRand=new Random();
        Random randNames=new Random(initRand.nextInt());
        Random randSurnames=new Random(initRand.nextInt());
        Pacient pacient=null;
        Integer maxId=null;
        
        for (int i = 0; i < numberPerson; i++) {
            
            int j=randNames.nextInt(listNames.length);
            String name= listNames[j];
            
            int k=randSurnames.nextInt(listSurname.length);
            String surname= listSurname[k];
            
            String rc=RcGenerator.generateRcForAge(10, 90, RcGenerator.Gender.MALE);
            
            maxId = DBgetData.selectMaxIdPacient(DBConnection.getInstance().getConnection());
            pacient=new Pacient(maxId+1, name, surname, rc);
            DBgetData.insertPacient(pacient, DBConnection.getInstance().getConnection());
            maxId++;
        }
    }
}


    
    

