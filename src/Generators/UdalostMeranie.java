/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Generators;

import Entity.TypMerania;

/**
 *
 * @author filip.silhar
 */
public class UdalostMeranie implements Comparable
{
    
    private UdalostMeranie next;
    private TypMerania typ;
    private long cas;
    private long interval;
    
    public UdalostMeranie(TypMerania typ, long cas)
    {
        this(typ,cas, (typ!=null ? typ.getInterval() : 60), null);
    }
    
    public UdalostMeranie(TypMerania typ, long cas,long interval)
    {
        this(typ,cas,interval,null);
    }

    public UdalostMeranie(TypMerania typ, long cas,long interval, UdalostMeranie next) {
        this.next = next;
        this.cas = cas;
        this.typ = typ;
        this.interval = interval;
    }

    public UdalostMeranie() {
        this(null,0,60,null);
    }

    @Override
    public int compareTo(Object o) {
        long rozdiel = (cas-((UdalostMeranie)o).getCas());
        if(rozdiel>Integer.MAX_VALUE)
            return Integer.MAX_VALUE;
        if(rozdiel<Integer.MIN_VALUE)
            return Integer.MIN_VALUE;
        return (int)rozdiel;
    }

    public void posunCas()
    {
        cas += typ.getInterval()*1000;
    }
    
    
    public UdalostMeranie getNext() {
        return next;
    }

    public long getCas() {
        return cas;
    }

    public TypMerania getTyp() {
        return typ;
    }            
    
    public void setNext(UdalostMeranie next) {
        this.next = next;
    }

    public void setCas(long cas) {
        this.cas = cas;
    }
    
    public void setTyp(TypMerania typ) {
        this.typ = typ;
    }
    
    
    
    
}
