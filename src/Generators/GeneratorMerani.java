package Generators;

import DatabaseCore.DBConnection;
import Entity.TypMerania;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Random;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author filip.silhar
 */
public class GeneratorMerani extends Thread {
    
    
    private Connection conn;
    private boolean generuj;
    private boolean koniec;
    private boolean realTime;
    private int pocetTypov;
    private TypMerania[] typMerania;
    private Random[] rand;
    private long startTime;
    private long currentTime;
    private UdalostMeranie prvaUdalost;
    private int pacient;
    private long pocetVygenerovanych;
    
    public GeneratorMerani()
    {
        generuj = false;
        koniec = false;
        realTime = false;
        startTime = System.currentTimeMillis();
        currentTime = 0;
        pacient = 0;
        pocetVygenerovanych = 0;
    }
    
    public boolean zapniGenerator(Connection conn, boolean realTime, long startTime, long currentTime, int pacientId)
    {
        try 
        {
            this.conn = this.conn == null ? DBConnection.getInstance().getConnection() : conn;
            if (this.conn==null)
                return false;
            nacitajTypy();
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        
        nastavParametreTypov();
        
        this.startTime = startTime<0 ? System.currentTimeMillis() : startTime;
        this.currentTime = currentTime;
        this.realTime = realTime;
        generuj = true;
        UdalostMeranie tmp = null;
        for(int x=pocetTypov-1;x>=0;x--)
        {
            tmp = new UdalostMeranie(typMerania[x], this.currentTime);
            tmp.setNext(prvaUdalost);
            prvaUdalost = tmp;
        }
        pacient = pacientId;
        pocetVygenerovanych = 0;
        return true;
    }

    
    private void nacitajTypy() throws SQLException
    {
        Statement stat=conn.createStatement();
        ResultSet rs = stat.executeQuery("SELECT * FROM typ_merania ORDER BY id_typ");
        TypMerania typ = null;
        rand = new Random[pocetTypov];
        Random r = new Random();
        if(typMerania==null)
            typMerania = new TypMerania[10];
        while(rs.next())
        {
            typ = new TypMerania(rs.getInt("id_typ"), rs.getString("popis"), rs.getInt("interval"), rs.getString("jednotka"),rs.getDouble("kriticka_dolna"), rs.getDouble("kriticka_horna"),rs.getDouble("chybna_dolna"), rs.getDouble("chybna_horna"),new Random(r.nextLong()),0,0);
            typMerania[pocetTypov] = typ;
            pocetTypov++;
            if(pocetTypov == typMerania.length)
            {
                TypMerania[] tmp = new TypMerania[typMerania.length + typMerania.length / 2];
                System.arraycopy(typMerania,0,tmp,0,pocetTypov);
                typMerania = tmp;
            }
        }
        rs.close();
        stat.close();
        
    }
    
    private void nastavParametreTypov()
    {
        for(int x=0;x<pocetTypov;x++)
        {
            if(typMerania[x].getPopis().equalsIgnoreCase("teplota"))
            {
                typMerania[x].setNasobicR(1);
                typMerania[x].setPosunR(36.5);
                typMerania[x].setKolisanieMax(0.1);
            }
            if(typMerania[x].getPopis().equalsIgnoreCase("systolicky_tlak"))
            {
                typMerania[x].setNasobicR(20);
                typMerania[x].setPosunR(120);
                typMerania[x].setKolisanieMax(1);
            }
            if(typMerania[x].getPopis().equalsIgnoreCase("diastolicky_tlak"))
            {
                typMerania[x].setNasobicR(20);
                typMerania[x].setPosunR(80);
                typMerania[x].setKolisanieMax(1);
            }
            if(typMerania[x].getPopis().equalsIgnoreCase("pulz"))
            {
                typMerania[x].setNasobicR(25);
                typMerania[x].setPosunR(70);
                typMerania[x].setKolisanieMax(0.5);
            }
            if(typMerania[x].getPopis().equalsIgnoreCase("sedimentacia"))
            {
                typMerania[x].setNasobicR(2);
                typMerania[x].setPosunR(5);
                typMerania[x].setKolisanieMax(2);
            }
            if(typMerania[x].getPopis().equalsIgnoreCase("leukocyty"))
            {
                typMerania[x].setNasobicR(1);
                typMerania[x].setPosunR(5);
                typMerania[x].setKolisanieMax(1);
            }
            if(typMerania[x].getPopis().equalsIgnoreCase("erytrocyty"))
            {
                typMerania[x].setNasobicR(0.5);
                typMerania[x].setPosunR(5);
                typMerania[x].setKolisanieMax(1);
            }
        }
    }
    
    
    @Override
    public void run() {
        //super.run(); //To change body of generated methods, choose Tools | Templates.
        UdalostMeranie aktUdal = null;
        Statement stat = null;
        String sql="";
        double hodnota=0;
        while(!koniec)
        {
            if(generuj)
            {
                try
                {
                    stat = conn.createStatement();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
            while(generuj)
            {
                aktUdal = prvaUdalost;
                if(realTime)
                {
                    try
                    {
                        for(int x=0;currentTime+x*1000<aktUdal.getCas();x++)
                        {
                            if(!generuj)
                                break;
                            sleep(1000);
                        }
                        
                        sleep((currentTime-aktUdal.getCas()) *1000);
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                }
                if(generuj)
                {
                    currentTime = aktUdal.getCas();
                    try
                    {
                        hodnota = aktUdal.getTyp().nextRandomDouble();
                        sql = "INSERT INTO meranie (cas,id_pacient,id_typ,id_vysetrenie,stav,hodnota) VALUES (";
                        sql += "to_date('" + toDateTime(startTime+currentTime) + "','DD.MM.YYYY/HH24:MI:SS'),";
                        sql += pacient + "," + aktUdal.getTyp().getId() + ",NULL,'" + aktUdal.getTyp().zaradHodnotu(hodnota) + "'," + String.format(Locale.ENGLISH,"%.2f", hodnota) + ")";

                    System.out.println(sql+"\n");
                    System.out.flush();

                        stat.execute(sql);
                        pocetVygenerovanych++;
                    }
                    catch(Exception e)
                    {
                        e.printStackTrace();
                    }
                    
                    prvaUdalost = aktUdal.getNext();
                    aktUdal.posunCas();
                    if(aktUdal.getCas()<prvaUdalost.getCas())
                    {
                        aktUdal.setNext(prvaUdalost);
                        prvaUdalost=aktUdal;
                    }
                    else
                    {
                        UdalostMeranie pom=prvaUdalost;
                        while(pom.getNext()!=null && aktUdal.getCas()>pom.getNext().getCas())
                        {
                            pom=pom.getNext();
                        }
                        aktUdal.setNext(pom.getNext());
                        pom.setNext(aktUdal);
                    }
                    
                }
            }
            if(stat!=null)
            {
                try
                {
                    if(stat!=null)
                        stat.close();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }

            try
            {
                sleep(100);
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
    }

    
    private String toDateTime(long millis)
    {
        Calendar now = new GregorianCalendar();
        now.setTimeInMillis(millis);
        String vysl="";
        int tmp=0;
        tmp = now.get(Calendar.DAY_OF_MONTH);
        if(tmp<10)
            vysl += "0";
        vysl += tmp + ".";
        tmp = now.get(Calendar.MONTH);
        tmp++;
        if(tmp<10)
            vysl += "0";
        vysl += tmp + ".";
        tmp = now.get(Calendar.YEAR);
        vysl += tmp + "/";
        
        tmp = now.get(Calendar.HOUR_OF_DAY);
        if(tmp<10)
            vysl += "0";
        vysl += tmp + ":";
        tmp = now.get(Calendar.MINUTE);
        if(tmp<10)
            vysl += "0";
        vysl += tmp + ":";
        tmp = now.get(Calendar.SECOND);
        if(tmp<10)
            vysl += "0";
        vysl += tmp;
        
        return vysl;
    }
    
    public boolean isGeneruj() {
        return generuj;
    }

    public boolean isKoniec() {
        return koniec;
    }

    public boolean isRealTime() {
        return realTime;
    }

    public void setGeneruj(boolean generuj) {
        this.generuj = generuj;
    }

    public void setKoniec(boolean koniec) {
        this.koniec = koniec;
    }

    public void setRealTime(boolean realTime) {
        this.realTime = realTime;
    }
    

    public long getPocetVygenerovanych() {
        return pocetVygenerovanych;
    }
    
    public static void main(String [] args)
    {
        GeneratorMerani gen=new GeneratorMerani();
        
        gen.start();
        while(!gen.isAlive())
        {
            yield();
        }
        gen.zapniGenerator(null, false, -1, 0, 1);
        
        /*try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        */
        while(gen.getPocetVygenerovanych()<100)
        {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        gen.setGeneruj(false);
        gen.setKoniec(true);
        
        System.out.println("pocet vygenerovanych: "+gen.getPocetVygenerovanych());
        /*while(gen.isAlive())
        {
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }*/
        
        /*try {
            Connection con = DBConnection.getInstance().getConnection();
            
            Statement stat = con.createStatement();
            
            ResultSet rs = stat.executeQuery("SELECT * FROM typ_merania");
            while (rs.next())
            {
                for(int x=0;x<8;x++)
                {
                    System.out.print(rs.getString(x+1)+"\t");
                }
                System.out.println();
            }
            rs.close();
                    
            
        } catch (SQLException ex) {
            ex.printStackTrace();
        }*/
    }

}
