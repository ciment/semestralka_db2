/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Gui;

/**
 *
 * @author rthr
 */
public class PomObj {
    private String znak;
    
    public PomObj(){
        
    }
    
    public PomObj (String znak){
        this.znak = znak;
    }

    public String getZnak() {
        return znak;
    }

    public void setZnak(String znak) {
        this.znak = znak;
    }
    
    public String toString(){
        return znak;
    }
}
