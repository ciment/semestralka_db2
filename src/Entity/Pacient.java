/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

/**
 *
 * @author Ciment
 */
public class Pacient {
    private int id;
    private String name;
    private String surname;
    private String RC;

    public Pacient(int id, String name, String surname, String RC) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.RC = RC;
    }
    
    public Pacient(){
        
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getRC() {
        return RC;
    }

    public void setRC(String RC) {
        this.RC = RC;
    }
    
    public String toString(){
        return id + " " + name + " " + surname;
    }

    public String getAll(){
        return id + " " + name + " " + surname + " " + RC;
    }
  
    
}