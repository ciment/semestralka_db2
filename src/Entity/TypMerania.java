/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Entity;

import java.util.Random;

/**
 *
 * @author filip.silhar
 */
public class TypMerania {
    
    private int id;
    private String popis;
    private int interval;
    private String jednotka;
    private double kritickaDolna;
    private double kritickaHorna;
    private double chybnaDolna;
    private double chybnaHorna;
    private Random rand;
    private double nasobicR;
    private double posunR;
    private double poslednaHodnota;
    private double kolisanieMax;

    
    public TypMerania(int id, String popis, int interval, String jednotka, double kritickaDolna, double kritickaHorna, double chybnaDolna, double chybnaHorna, Random random, double naosbic, double posun) {
        this.id = id;
        this.popis = popis;
        this.interval = interval;
        this.jednotka = jednotka;
        this.kritickaDolna = kritickaDolna;
        this.kritickaHorna = kritickaHorna;
        this.chybnaDolna = chybnaDolna;
        this.chybnaHorna = chybnaHorna;
        this.rand = random;
        poslednaHodnota = (kritickaDolna+kritickaHorna)/2.0;
        this.kolisanieMax = Math.pow(interval, 1/4)/100;
    }
    
    public TypMerania(int id, String popis, int interval, String jednotka, double kritickaDolna, double kritickaHorna, double chybnaDolna, double chybnaHorna) {
        this.id = id;
        this.popis = popis;
        this.interval = interval;
        this.jednotka = jednotka;
        this.kritickaDolna = kritickaDolna;
        this.kritickaHorna = kritickaHorna;
        this.chybnaDolna = chybnaDolna;
        this.chybnaHorna = chybnaHorna;
        
    }
    
    public TypMerania(){
        
    }
    
     public TypMerania(int id){
        this.id=id;
    }

    
    public char zaradHodnotu(double hodnota)
    {
        if(hodnota < chybnaDolna || hodnota > chybnaHorna)
            return 'E';
        if(hodnota < kritickaDolna || hodnota > kritickaHorna)
            return 'C';
        return 'G';
    }
    
    
    public double nextRandomDouble()
    {
        double vysl = rand.nextGaussian()*nasobicR + posunR;
        if(Math.abs(vysl-poslednaHodnota)>kolisanieMax*posunR)
        {
            if(vysl<poslednaHodnota)
                vysl = poslednaHodnota - posunR*kolisanieMax;
            else
                vysl = poslednaHodnota + posunR*kolisanieMax;
        }
        poslednaHodnota = vysl;
        return vysl;
    }
    
    public int getId() {
        return id;
    }

    public String getPopis() {
        return popis;
    }

    public int getInterval() {
        return interval;
    }

    public String getJednotka() {
        return jednotka;
    }

    public double getKritickaDolna() {
        return kritickaDolna;
    }

    public double getKritickaHorna() {
        return kritickaHorna;
    }

    public double getChybnaDolna() {
        return chybnaDolna;
    }

    public double getChybnaHorna() {
        return chybnaHorna;
    }
    
    public Random getRandom() {
        return rand;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPopis(String popis) {
        this.popis = popis;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public void setJednotka(String jednotka) {
        this.jednotka = jednotka;
    }

    public void setKritickaDolna(double kritickaDolna) {
        this.kritickaDolna = kritickaDolna;
    }

    public void setKritickaHorna(double kritickaHorna) {
        this.kritickaHorna = kritickaHorna;
    }

    public void setChybnaDolna(double chybnaDolna) {
        this.chybnaDolna = chybnaDolna;
    }

    public void setChybnaHorna(double chybnaHorna) {
        this.chybnaHorna = chybnaHorna;
    }
    
    public void setRandom(Random random) {
        rand = random;
    }

    public double getNasobicR() {
        return nasobicR;
    }

    public double getPosunR() {
        return posunR;
    }

    public void setNasobicR(double nasobicR) {
        this.nasobicR = nasobicR;
    }

    public void setPosunR(double posunR) {
        this.posunR = posunR;
    }

    public double getPoslednaHodnota() {
        return poslednaHodnota;
    }

    public double getKolisanieMax() {
        return kolisanieMax;
    }

    public void setPoslednaHodnota(double poslednaHodnota) {
        this.poslednaHodnota = poslednaHodnota;
    }

    public void setKolisanieMax(double kolisanieMax) {
        this.kolisanieMax = kolisanieMax;
    }
    
    
    public String toString(){
        return popis;
    }
}
