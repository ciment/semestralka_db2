/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entity;

import java.util.Date;

/**
 *
 * @author rthr
 */
public class Meranie {
    private Date cas; //date typ
    private int id_pacient;
    private int id_typ;
    private int id_vysetrenie;
    private String stav;
    private float hodnota;

    public Meranie(Date cas, int id_pacient, int id_typ, int id_vysetrenie, String stav, float hodnota) {
        this.cas = cas;
        this.id_pacient = id_pacient;
        this.id_typ = id_typ;
        this.id_vysetrenie = id_vysetrenie;
        this.stav = stav;
        this.hodnota = hodnota;
    }
    
    public Meranie(Date cas, String stav, float hodnota){
        this.cas = cas;
        this.stav = stav;
        this.hodnota = hodnota;
    }
    
    public Meranie(){
        
    }

    public Date getCas() {
        return cas;
    }

    public float getHodnota() {
        return hodnota;
    }

    public int getId_pacient() {
        return id_pacient;
    }

    public int getId_typ() {
        return id_typ;
    }

    public int getId_vysetrenie() {
        return id_vysetrenie;
    }

    public String getStav() {
        return stav;
    }

  
    
    public String toString(){
        String str = "";
		
        str = String.format("%20s%10s%10s", cas, stav,hodnota);
        return str;        
        //return cas + "     " + stav + "     " + hodnota;
    }

    
}
