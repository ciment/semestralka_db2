/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Entity;

/**
 *
 * @author rthr
 */
public class Vysetrenie {
    private int id_vysetrenie;
    private String popis;
    
    
    
    public  Vysetrenie(int id_vys, String popis){
        this.id_vysetrenie = id_vys;
        this.popis = popis;
    }
    
    public  Vysetrenie(){
        
    }

    public int getId_vysetrenie() {
        return id_vysetrenie;
    }

    public String getPopis() {
        return popis;
    }
    
    public String toString(){
        return popis;
    }
    
}
