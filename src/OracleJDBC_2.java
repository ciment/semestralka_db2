
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


// druha moznost ako sa pripojit na oracle databasu
public class OracleJDBC_2 {
 
	public static void main(String[] argv) {
 
		System.out.println("-------- Oracle JDBC Connection Testing ------");
 
		try {
 
			Class.forName("oracle.jdbc.driver.OracleDriver");
 
		} catch (ClassNotFoundException e) {
 
			System.out.println("Where is your Oracle JDBC Driver?");
			e.printStackTrace();
			return;
 
		}
 
		System.out.println("Oracle JDBC Driver Registered!");
 
		Connection connection = null;
                 PreparedStatement preparedStatement = null;
		try {
 
			connection = DriverManager.getConnection(
					"jdbc:oracle:thin:@asterix.fri.uniza.sk:1521:orcl", "hriadel",
					"asdfg");
                        
                        String statement="SELECT * FROM os_udaje";
                        
                        preparedStatement=connection.prepareStatement(statement);
                    ResultSet executeQuery = preparedStatement.executeQuery();
                 
 
		} catch (SQLException e) {
 
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
 
		}
 
		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
	}
 
}